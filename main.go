package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type playerStruct struct {
	port int
}
type coordonnee struct {
	x int
	y int
}

type boatStruct struct {
	originX     int
	originY     int
	direction   int
	length      int
	leftSize    int
	coordonnees []coordonnee
}

var boardPublic [10][10]string
var boardPrivate [10][10]string
var boatsList []boatStruct
var players []playerStruct

func notAllowed(w http.ResponseWriter, req *http.Request) {
	response(http.StatusMethodNotAllowed, w, req.Method+" is not allowed.")
}

func badRequest(w http.ResponseWriter, message string) {
	response(http.StatusBadRequest, w, message)
}

func response(status int, w http.ResponseWriter, message string) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, message)
}

func board(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		notAllowed(w, req)
	} else {
		var result string

		for _, v := range boardPublic {
			for _, w := range v {
				if w == "1" {
					result += "🚢 "
				} else if w == "2" {
					result += "🔥 "
				} else {
					result += "🌊 "
				}

			}

			result += "\n"
		}

		response(200, w, result)
	}
}

func boats(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		notAllowed(w, req)
	} else {
		response(200, w, strconv.FormatInt(int64(len(boatsList)), 10))
	}
}

func initialise(boardGame [10][10]string) [10][10]string {
	for i, v := range boardGame {
		for y, _ := range v {
			boardGame[i][y] = "0"
		}
	}

	return boardGame
}

func addBoat(size int) boatStruct {
	direction := rand.Intn(2)
	originX := rand.Intn(10 - size)
	originY := rand.Intn(10 - size)
	leftSize := 4
	var coordonnees []coordonnee
	var point coordonnee

	for i := 0; i < size; i++ {
		if direction == 0 {
			if boardPrivate[originX+i][originY] != "0" {
				point.x = originX + i
				point.y = originY
				coordonnees = append(coordonnees, point)
				fmt.Println(coordonnees)
				return addBoat(size)
			}
		} else {
			if boardPrivate[originX][originY+i] != "0" {
				point.x = originX
				point.y = originY + i
				coordonnees = append(coordonnees, point)
				return addBoat(size)
			}
		}
	}

	boat := boatStruct{
		originX:     originX,
		originY:     originY,
		direction:   direction,
		length:      size,
		leftSize:    leftSize,
		coordonnees: coordonnees,
	}

	for i := 0; i < size; i++ {
		if direction == 0 {
			boardPrivate[originX+i][originY] = "1"
		} else {
			boardPrivate[originX][originY+i] = "1"
		}
	}

	return boat
}

func addBoats(quantity int, size int) {
	for i := 0; i < quantity; i++ {
		boat := addBoat(size)
		boatsList = append(boatsList, boat)
	}
}

func updateBoatSize(x int, y int) {
	for i := 0; i < len(boatsList); i++ {
		if boatsList[i].originX == x && boatsList[i].originY == y {
			if boatsList[i].leftSize == 0 {
				boatsList = boatsList[0 : len(boatsList)-1]
			} else {
				boatsList[i].leftSize--
			}
		}
	}
}

func hit(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		notAllowed(w, req)
	} else {
		req.ParseForm()

		xHit := req.Form.Get("xHit")
		yHit := req.Form.Get("yHit")
		xHitValue, _ := strconv.Atoi(xHit)
		yHitValue, _ := strconv.Atoi(yHit)

		if len(xHit) > 0 && len(yHit) > 0 {
			if boardPublic[xHitValue][yHitValue] == "1" {
				boardPublic[xHitValue][yHitValue] = "2"
				updateBoatSize(xHitValue, yHitValue)
				if boatsList[0].leftSize == 0 {
					boatsList = boatsList[0 : len(boatsList)-1]
					fmt.Println("Coulé")
				} else {
					boatsList[0].leftSize--
					fmt.Println("Touché")
				}

			} else {
				fmt.Println("Un coup dans l'eau...")
			}
			endGame()
		} else {
			badRequest(w, "Missing parameters")
		}
	}
}

func scanUserInformation() playerStruct {
	var player playerStruct
	fmt.Println("Port : ")
	fmt.Scanf("%d", &player.port)
	return player
}

func gamePlayers() []playerStruct {
	var players []playerStruct
	var player playerStruct
	var entry int
	fmt.Println(" Entrez le numéro de port sur lequel vous souhaitez jouer : ")
	fmt.Scanf("%d", &player.port)
	players = append(players, player)
	fmt.Println("Entrez le nombre de joueurs qui joueront avec vous  : ")

	fmt.Scanf("%d", &entry)

	for i := 0; i < entry; i++ {
		fmt.Println("Informations du joueur n° ", i+1)
		players = append(players, scanUserInformation())
	}

	return players
}

func endGame() bool {
	var end bool
	end = true
	for _, v := range boardPublic {
		for _, w := range v {
			if w == "1" {
				end = false
			}
		}
	}
	if end == true {
		panic("C'est fini mon reuf")
	}
	return end
}

func main() {
	rand.Seed(time.Now().UnixNano())
	players = gamePlayers()

	boardPublic = initialise(boardPublic)
	boardPrivate = initialise(boardPrivate)

	addBoats(2, 2)

	boardPublic = boardPrivate

	http.HandleFunc("/board", board)
	http.HandleFunc("/boats", boats)
	http.HandleFunc("/hit", hit)

	log.Println("Server listening on http://localhost:" + strconv.Itoa(players[0].port))
	server := &http.Server{Addr: ":" + strconv.Itoa(players[0].port), Handler: nil}

	if err := server.ListenAndServe(); err != nil {
		log.Fatal("Server failed to start.")
	}
}
